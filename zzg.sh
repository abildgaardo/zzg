#!/bin/bash
# zzg
# By Thomas Abildgaard
# thomas@abildgaard.com
# http://abildgaard.com
# =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
# zzg is inspired by both 'blop' and 'statix' static web site generators.
# Kudo's goes out to Drew and plugnburn
# =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
POSTDIR="$PWD/mdpages"
OUTDIR="$PWD/output"
TPLDIR="$PWD/template"
declare -A POSTS
#declare -A METADATA
STATIC_PAGES=("index.md" "projects.md" "misc.md" "about.md")
TPLFILE="$TPLDIR/index.tpl"
MENUFILE="$TPLDIR/menu.tpl"
ARCHIVE_TEMP="$PWD/archive_temp.txt"
MAX_POSTS="10"
DATE_FORMAT="%Y-%m-%d"
# Internal Field Separator
IFSBAK="$IFS"
#=================(dont put your password in this file)=======================
FTP_SERV=ftp.simply.com
FTP_LOGIN=abildgaardo
FTP_PORT=21
FTP_SSL=yes
#=============================================================================
FTP_CONNECT="ftp - $FTP_SERV -l $FTP_LOGIN"

function init {
	if [[! -d "$POSTDIR" ]]; then
		mkdir $POSTDIR
	fi
	if [[! -d "$OUTDIR" ]]; then
		mkdir $OUTDIR
	fi
	if [[! -d "$TPLDIR" ]]; then
		mkdir $TPLDIR
	fi
}

if [[ ! -d "$POSTDIR" ]]; then
    echo "posts dir does not exist"
    exit
fi

if [[ ! -d "$OUTDIR" ]]; then
    echo "HTML output dir: ${OUTDIR} does not exist, attempting to create dir"
    mkdir "$OUTDIR"
fi


function syncToFTP {

echo "test";
}

function slug {
	tr -cs '[:alnum:]\n' - | tr '[:upper:]' '[:lower:]' | sed 's|^-*||;s|-*$||'
}

function newPost {
    readonly TITLE=$@
    readonly post_file="$POSTDIR/$(date +%Y-%m-%d)-$(slug <<< "$TITLE").md"
# Writing a template before editing new post	    
cat > "$post_file" <<-EOF
---
title:$TITLE
date:$(date -u +%FT%TZ)
layout:post
draft:true
summary:
---
EOF
	[[ "$EDITOR" ]] && $EDITOR "$post_file"
}

function getMetaData
{
    local MTAG=''
    
    MDATA=$(grep -Po '^([a-z]+:[a-zA-Z0-9 -]+)$' "$1")

    for MTAG in $MDATA; do
        METADATA["${MTAG%%:*}"]=${MTAG#*:}
    done
}

function displayMetaData 
{
    for Q in "${!METADATA[@]}"; do 
        echo "META ${Q} --- ${METADATA[$Q]}" 
    done
}

function clearHtml
{
    echo "wasnt me didnt delete anything"
#  rm -f -R "${OUTDIR}/" !("_static")
}
#
# $1 post markdown file
#
function renderPost {

    local MD="$(<$1)"
    # Replace KEY with VALUE from metadata.  
    for K in "${!METADATA[@]}"; do
        TPLTXT="${TPLTXT//\[\[@${K}\]\]/${METADATA[$K]}}"
    done
    
    # Generering af HTML ud fra markdown
    # sed fjerner Frontmatter fra markdown fil 
    MDHTML=$(echo -n "$MD"| sed '1,/---/d' | markdown )
    TPLTXT="${TPLTXT//\[\[@content\]\]/${MDHTML}}"
    TPLTXT="${TPLTXT//\[\[@menu\]\]/${MENUTXT}}"
    SLUG=${METADATA['slug']}
    # Storing post in archive temp file
    # echo "${METADATA["date"]}.$RANDOM:${METADATA["slug"]}:${METADATA["title"]}" >> $ARCHIVE_TEMP
    echo "rendering page: (${SLUG}) ${1}"

    if [ ! -e "$OUTDIR/$SLUG" ]; then 
        mkdir "$OUTDIR/$SLUG"
        echo "Creating dir: $OUTDIR/$SLUG"
    fi

    if [ $SLUG = 'index' ]; then
        echo $TPLTXT >"$OUTDIR/"index.html
    else
        echo $TPLTXT >"$OUTDIR/$SLUG/"index.html
    fi
}

case $1 in
    post)
        newPost "${@:2}" ;;
    clear)
        clearHtml ;;
    sync)
        syncToFto ;;
    gen)
        readarray -t pages < <(find "$POSTDIR" -name '*.md' | sort -r)
	    COUNTER=0
	for post in "${pages[@]}"; do
        id="$(basename "$post" .md)"
   
        TPLTXT="$(<$TPLFILE)"
        MENUTXT="$(<$MENUFILE)"

        # GRAB METADATA FROM POST
        unset METADATA
        declare -A METADATA 
        getMetaData $post 
	    
        # skip drafts
        [[ $METADATA['draft'] ]] || continue
        
        renderPost $post
	
		(( COUNTER++ ))
	done
	#render_file "$LAYOUT_DIR/index.md" > "$OUTPUT_DIR/index.html" &

        echo "$COUNTER page(s) written to output"
    ;;
esac
